$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "sate_auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "sate_auth"
  s.version     = SateAuth::VERSION
  s.authors     = ["Samuel Teshome"]
  s.email       = ["samuailteshome@yahoo.com"]
  s.homepage    = "http://www.sate.com.et"
  s.summary     = "Core authentication module."
  s.description = "This engine contains the core authentication module for future applications."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.5"

  s.add_dependency "bcrypt", "~> 3.1", ">=3.1.7"

  s.add_development_dependency "sqlite3"
end
